DROP SCHEMA IF EXISTS sic CASCADE;
CREATE SCHEMA sic;

SET search_path TO sic;

CREATE TABLE marca (

	id BIGSERIAL PRIMARY KEY NOT NULL,
	nombre VARCHAR(255) UNIQUE NOT NULL
);

INSERT INTO marca(id, nombre) VALUES 
(1, 'HP'),
(2, 'Dell'),
(3, 'Asus'),
(4, 'Lenovo'),
(5, 'Mac');

CREATE TABLE formulario (

	id BIGSERIAL PRIMARY KEY NOT NULL,
	nombre VARCHAR(255) NOT NULL,
	apellido VARCHAR(255) UNIQUE NOT NULL,
	id_marca BIGINT NOT NULL,
	fecha TIMESTAMP NOT NULL,
	correo VARCHAR(255) NOT NULL,
	
	FOREIGN KEY (id_marca) REFERENCES marca(id)
);

CREATE TABLE usuarios (

	id BIGSERIAL PRIMARY KEY NOT NULL,
	usuario VARCHAR(255) NOT NULL,
	pass VARCHAR(255) UNIQUE NOT NULL
);

INSERT INTO usuarios(usuario, pass) VALUES 
('admin', '25175000'),
('user', '13001000');

