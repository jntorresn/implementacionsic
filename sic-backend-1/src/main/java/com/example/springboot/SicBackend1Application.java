package com.example.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SicBackend1Application {

	public static void main(String[] args) {
		SpringApplication.run(SicBackend1Application.class, args);
	}

}
