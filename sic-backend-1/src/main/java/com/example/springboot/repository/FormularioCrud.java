package com.example.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.springboot.model.Formulario;

public interface FormularioCrud extends JpaRepository<Formulario, Long> {

}
