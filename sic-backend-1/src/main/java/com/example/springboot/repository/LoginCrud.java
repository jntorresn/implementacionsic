package com.example.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.springboot.model.Usuarios;

public interface LoginCrud extends JpaRepository<Usuarios, Long> {
	
	@Query(value = "SELECT us FROM Usuarios us WHERE us.usuario = :nombre AND us.pass = :pass", nativeQuery = false)
	Usuarios obtenerPersona(String nombre, String pass);


}
