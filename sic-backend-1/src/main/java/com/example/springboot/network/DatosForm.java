package com.example.springboot.network;

public class DatosForm {

	private String nombre;
	private String apellido;
	private String marcaPC;
	private String fechaYHora;
	private String correo;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getMarcaPC() {
		return marcaPC;
	}

	public void setMarcaPC(String marcaPC) {
		this.marcaPC = marcaPC;
	}

	public String getFechaYHora() {
		return fechaYHora;
	}

	public void setFechaYHora(String fechaYHora) {
		this.fechaYHora = fechaYHora;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

}
