package com.example.springboot.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.network.Response;
import com.example.springboot.services.VerificarLoginService;

@CrossOrigin(origins = { "*" }, methods = { RequestMethod.GET, RequestMethod.POST })
@RestController
@RequestMapping("/api/sic")
public class LoginRest {
	
	@Autowired
	private VerificarLoginService verificarLogin;
	
	@PostMapping(path = "/login")
	public Response login(String user, String pass) {

		System.out.println("Correcto");
		
		String resultadoLogin = verificarLogin.verificarLogin(user, pass);
		
		if(resultadoLogin.equals("Correcto")) {
			
			return new Response("Correcto");
			
		} else {
			
			return new Response("Error");
			
		}
	}

	

}
