package com.example.springboot.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.network.DatosForm;
import com.example.springboot.network.Response;
import com.example.springboot.services.GuardarFormService;

import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin(origins = { "*" }, methods = { RequestMethod.GET, RequestMethod.POST })
@RestController
@RequestMapping("/api/sic")
public class ProcesarFormRest {
	
	@Autowired
	GuardarFormService guardarForm;

	@PostMapping("/procesarForm")
	public Response validarArchivo(DatosForm datos) {
		
		String resultadoGuardarFormulario = guardarForm.guardarFormulario(datos);

		if (resultadoGuardarFormulario.equals("Correcto")) {

			return new Response("Correcto");

		} else {

			return new Response("Error");

		}

	}

}
