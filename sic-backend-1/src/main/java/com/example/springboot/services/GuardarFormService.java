package com.example.springboot.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

import com.example.springboot.model.Formulario;
import com.example.springboot.network.DatosForm;
import com.example.springboot.repository.FormularioCrud;

@Service
public class GuardarFormService {

	@Autowired
	private FormularioCrud formularioCrud;

	public String guardarFormulario(DatosForm datos) {

		Formulario formulario = new Formulario(datos.getNombre(), datos.getApellido(), datos.getMarcaPC(),
				new Timestamp(System.currentTimeMillis()), datos.getCorreo());

		formularioCrud.save(formulario);

		return "Correcto";
	}

}
