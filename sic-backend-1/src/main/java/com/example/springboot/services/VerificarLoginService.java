package com.example.springboot.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springboot.model.Usuarios;
import com.example.springboot.repository.LoginCrud;

@Service
public class VerificarLoginService {

	@Autowired
	private LoginCrud loginCrud;

	public String verificarLogin(String user, String pass) {

		Usuarios usuario = loginCrud.obtenerPersona(user, pass);

		if (usuario == null) {

			return "Error";
		} else {
			return "Correcto";

		}

	}

}
