

$("#toggle-button").click(function () {


    if ($(window).width() < 1005) {

        $(".side-menu").toggleClass("side-menu-expanded");

    } else {

        if ($("#side-menu").hasClass("side-menu-collapsed")) {

            $("#side-menu").removeClass("side-menu-collapsed");
            $(".sidebar-title, .right-icon, .side-text").show();
            $("#main-menu-title").removeClass("sidebar-subtitile-selected");
            $("#second-menu-title").addClass("sidebar-subtitile-selected");
            $(".container-elements").removeClass("container-expanded");
            $("#main-menu-container").removeClass("dropdown-menu right-menu").show();
            $("#second-menu-container").removeClass("dropdown-menu right-menu").show();
            $(".sidebar-subtitile-hidden").hide();
            $("#main-menu").addClass("side-menu-hidden").hide();
            $("#second-menu").addClass("side-menu-hidden").hide();
            $("#third-menu").addClass("side-menu-hidden").hide();
            $("#fourth-menu").addClass("side-menu-hidden").hide();
            $("#fifth-menu").addClass("side-menu-hidden").hide();
            $("#sixth-menu").addClass("side-menu-hidden").hide();
            $("#seventh-menu").addClass("side-menu-hidden").hide();
            $(".side-list").removeClass("side-list-unlisted");

        } else {

            $("#side-menu").addClass("side-menu-collapsed");
            $(".sidebar-title, .right-icon, .side-text").hide();
            $("#main-menu-title").removeClass("sidebar-subtitile-selected");
            $("#second-menu-title").removeClass("sidebar-subtitile-selected");
            $(".container-elements").addClass("container-expanded");
            $("#main-menu-container").addClass("dropdown-menu right-menu").hide();
            $("#second-menu-container").addClass("dropdown-menu right-menu").hide();
            $(".sidebar-subtitile-hidden").show();
            $("#main-menu").removeClass("side-menu-hidden").show();
            $("#second-menu").removeClass("side-menu-hidden").show();
            $("#third-menu").removeClass("side-menu-hidden").show();
            $("#fourth-menu").removeClass("side-menu-hidden").show();
            $("#fifth-menu").removeClass("side-menu-hidden").show();
            $("#sixth-menu").removeClass("side-menu-hidden").show();
            $("#seventh-menu").removeClass("side-menu-hidden").show();
            $(".side-list").addClass("side-list-unlisted");
        }

    }
});