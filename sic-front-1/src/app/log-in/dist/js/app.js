// ---- ANGULAR ---- //

angular.module("sicc", [
    "ngRoute",
    "datatables",
    "datatables.columnfilter",
    "ngNotify",
    "ui.calendar",
    "uiGmapgoogle-maps"
]);


angular.module("sicc").config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "main-menu.html",
            controller: "main-menu"
        })
        .when("/principal", {
            templateUrl: "main-menu.html",
            controller: "main-menu"
        })
        .when("/maintenance", {
            templateUrl: "maintenance/maintenance.html",
            controller: "maintenance"
        })
        .when("/road-work", {
            templateUrl: "maintenance/Road-Work/road-work.html",
            controller: "road-work"
        })
        .when("/new-road-work", {
            templateUrl: "maintenance/Road-Work/new-road-work.html",
            controller: "new-road-work"
        })
        .when("/road-work-info", {
            templateUrl: "maintenance/Road-Work/road-work-info.html",
            controller: "road-work-info"
        })
        .when("/inventory-maintenance", {
            templateUrl: "maintenance/inventory/inventory-maintenance.html",
            controller: "inventory-maintenance"
        })
        .when("/inventory-maintenance-info", {
            templateUrl: "maintenance/inventory/inventory-maintenance-info.html",
            controller: "inventory-maintenance-info"
        })
        .when("/new-inventory-maintenance", {
            templateUrl: "maintenance/inventory/new-inventory-maintenance.html",
            controller: "new-inventory-maintenance"
        })
        .when("/notifications", {
            templateUrl: "notification/notifications.html",
            controller: "notifications"
        })
        .when("/calendar", {
            templateUrl: "calendar/calendar.html",
            controller: "calendar"
        })
        .when("/calendar-maintenance", {
            templateUrl: "calendar/maintenance/calendar-maintenance.html",
            controller: "calendar-maintenance"
        })
        .when("/GIS", {
            templateUrl: "GIS/GIS.html",
            controller: "GIS"
        })
        .when("/gis-maintenance", {
            templateUrl: "GIS/maintenance/gis-maintenance.html",
            controller: "gis-maintenance"
        })
        .when("/gis-inventory", {
            templateUrl: "GIS/inventory/gis-inventory.html",
            controller: "gis-inventory"
        })
        .when("/gis-events", {
            templateUrl: "GIS/events/gis-events.html",
            controller: "gis-events"
        })
        .when("/mobility", {
            templateUrl: "GIS/mobility/mobility.html",
            controller: "mobility"
        })
        .when("/inventory", {
            templateUrl: "inventory/inventory.html",
            controller: "inventory"
        })
        .when("/vehicles", {
            templateUrl: "inventory/vehicle/vehicles.html",
            controller: "vehicles"
        })
        .when("/new-vehicle", {
            templateUrl: "inventory/vehicle/new-vehicle.html",
            controller: "new-vehicle"
        })
        .when("/vehicle-info", {
            templateUrl: "inventory/vehicle/vehicle-info.html",
            controller: "vehicle-info"
        })
        .when("/civil-work", {
            templateUrl: "inventory/civil-work/civil-work.html",
            controller: "civil-work"
        })
        .when("/new-civil-work", {
            templateUrl: "inventory/civil-work/new-civil-work.html",
            controller: "new-civil-work"
        })
        .when("/civil-work-info", {
            templateUrl: "inventory/civil-work/civil-work-info.html",
            controller: "civil-work-info"
        })
        .when("/instrument", {
            templateUrl: "inventory/instrument/instrument.html",
            controller: "instrument"
        })
        .when("/new-instrument", {
            templateUrl: "inventory/instrument/new-instrument.html",
            controller: "new-instrument"
        })
        .when("/instrument-info", {
            templateUrl: "inventory/instrument/instrument-info.html",
            controller: "instrument-info"
        })
        .when("/place", {
            templateUrl: "inventory/place/place.html",
            controller: "place"
        })
        .when("/new-place", {
            templateUrl: "inventory/place/new-place.html",
            controller: "new-place"
        })
        .when("/place-info", {
            templateUrl: "inventory/place/place-info.html",
            controller: "place-info"
        })
        .when("/sign", {
            templateUrl: "inventory/sign/sign.html",
            controller: "sign"
        })
        .when("/new-sign", {
            templateUrl: "inventory/sign/new-sign.html",
            controller: "new-sign"
        })
        .when("/sign-info", {
            templateUrl: "inventory/sign/sign-info.html",
            controller: "sign-info"
        })
        .when("/ITS", {
            templateUrl: "inventory/ITS/ITS.html",
            controller: "ITS"
        })
        .when("/new-ITS", {
            templateUrl: "inventory/ITS/new-ITS.html",
            controller: "new-ITS"
        })
        .when("/ITS-info", {
            templateUrl: "inventory/ITS/ITS-info.html",
            controller: "ITS-info"
        })
        .when("/operation", {
            templateUrl: "operation/operation.html",
            controller: "operation"
        })
        .when("/event", {
            templateUrl: "operation/event/event.html",
            controller: "event"
        })
        .when("/event-info", {
            templateUrl: "operation/event/event-info.html",
            controller: "event-info"
        })
        .when("/new-event", {
            templateUrl: "operation/event/new-event.html",
            controller: "new-event"
        })
        .when("/obstacle", {
            templateUrl: "operation/obstacle/obstacle.html",
            controller: "obstacle"
        })
        .when("/obstacle-info", {
            templateUrl: "operation/obstacle/obstacle-info.html",
            controller: "obstacle-info"
        })
        .when("/supervision", {
            templateUrl: "supervision/supervision.html",
            controller: "supervision"
        })
        .when("/indicators", {
            templateUrl: "supervision/indicators/indicators.html",
            controller: "indicators"
        })
        .when("/new-measure", {
            templateUrl: "supervision/indicators/measure/new-measure.html",
            controller: "new-measure"
        })
        .when("/IRI-form", {
            templateUrl: "supervision/indicators/measure/E1-IRI/IRI-form.html",
            controller: "IRI-form"
        })
        .when("/rutting-form", {
            templateUrl: "supervision/indicators/measure/E2-rutting/rutting-form.html",
            controller: "rutting-form"
        })
        .when("/fissure-form", {
            templateUrl: "supervision/indicators/measure/E3-fissures/fissure-form.html",
            controller: "fissure-form"
        })
        .when("/transverse-friction-form", {
            templateUrl: "supervision/indicators/measure/E4-transverse-friction/transverse-friction-form.html",
            controller: "transverse-friction-form"
        })
        .when("/texture-form", {
            templateUrl: "supervision/indicators/measure/E5-texture/texture-form.html",
            controller: "texture-form"
        })
        .when("/bump-form", {
            templateUrl: "supervision/indicators/measure/E6-bump/bump-form.html",
            controller: "bump-form"
        })
        .when("/sinking-form", {
            templateUrl: "supervision/indicators/measure/E7-sinking/sinking-form.html",
            controller: "sinking-form"
        })
        .when("/margins-form", {
            templateUrl: "supervision/indicators/measure/E8-margins/margins-form.html",
            controller: "margins-form"
        })
        .when("/drainage-form", {
            templateUrl: "supervision/indicators/measure/E10-drainage/drainage-form.html",
            controller: "drainage-form"
        })
        .when("/vertical-sign-form", {
            templateUrl: "supervision/indicators/measure/E11-vertical-sign/vertical-sign-form.html",
            controller: "vertical-sign-form"
        })
        .when("/horizontal-sign-form", {
            templateUrl: "supervision/indicators/measure/E12-horizontal-sign/horizontal-sign-form.html",
            controller: "horizontal-sign-form"
        })
        .when("/containment-elements-form", {
            templateUrl: "supervision/indicators/measure/E13-containment-elements/containment-elements-form.html",
            controller: "containment-elements-form"
        })
        .when("/ilumination-form", {
            templateUrl: "supervision/indicators/measure/E14-ilumination/ilumination-form.html",
            controller: "ilumination-form"
        })
        .when("/structure-form", {
            templateUrl: "supervision/indicators/measure/E15-structures/structure-form.html",
            controller: "structure-form"
        })
        .when("/structural-capacity-form", {
            templateUrl: "supervision/indicators/measure/E16-structural-capacity/structural-capacity-form.html",
            controller: "structural-capacity-form"
        })
        .when("/cracks-form", {
            templateUrl: "supervision/indicators/measure/E18-cracks/cracks-form.html",
            controller: "cracks-form"
        })
        .when("/staggering-form", {
            templateUrl: "supervision/indicators/measure/E19-staggering/staggering-form.html",
            controller: "staggering-form"
        })
        .when("/load-transfer-form", {
            templateUrl: "supervision/indicators/measure/E20-load-transfer/load-transfer-form.html",
            controller: "load-transfer-form"
        })
        .when("/joints-form", {
            templateUrl: "supervision/indicators/measure/E21-joints/joints-form.html",
            controller: "joints-form"
        })
        .when("/chipping-form", {
            templateUrl: "supervision/indicators/measure/E22-chipping/chipping-form.html",
            controller: "chipping-form"
        })
        .when("/ocupation-form", {
            templateUrl: "supervision/indicators/measure/O2-ocupation/ocupation-form.html",
            controller: "ocupation-form"
        })
        .when("/toll-line-form", {
            templateUrl: "supervision/indicators/measure/O3-toll-line/toll-line-form.html",
            controller: "toll-line-form"
        })
        .when("/IRI-info", {
            templateUrl: "supervision/indicators/info/E1-IRI/indicator-info.html",
            controller: "IRI-info"
        })
        .when("/rutting-info", {
            templateUrl: "supervision/indicators/info/E2-rutting/indicator-info.html",
            controller: "rutting-info"
        })
        .when("/fissure-info", {
            templateUrl: "supervision/indicators/info/E3-fissures/indicator-info.html",
            controller: "fissures-info"
        })
        .when("/transverse-friction-info", {
            templateUrl: "supervision/indicators/info/E4-transverse-friction/indicator-info.html",
            controller: "transverse-friction-info"
        })
        .when("/texture-info", {
            templateUrl: "supervision/indicators/info/E5-texture/indicator-info.html",
            controller: "texture-info"
        })
        .when("/bump-info", {
            templateUrl: "supervision/indicators/info/E6-bump/indicator-info.html",
            controller: "bump-info"
        })
        .when("/sinking-info", {
            templateUrl: "supervision/indicators/info/E7-sinking/indicator-info.html",
            controller: "sinking-info"
        })
        .when("/margins-info", {
            templateUrl: "supervision/indicators/info/E8-margins/indicator-info.html",
            controller: "margins-info"
        });
});

angular.module("sicc").factory("principal", function ($http, $window) {

    return {

        checkSession: function () {

            var user = new Object();

            return $http.get("../sections/session-getter.php").then(function (response) {

                user.image = "../" + response.data.photo;
                user.name = response.data.name;
                user.type = response.data.userType;
                user.position = response.data.position;
                user.entity = response.data.entity;

                if (response.data == "Close") {
                    $("#logout-session").submit();
                    return "";
                } else {

                    return user;
                }
            });
        },

        checkNews: function () {

            var news = new Object();

            return $http({
                url: "../sections/notification/get-notification.php",
                method: "GET",
                params: {
                    complete: false
                }
            }).then(function (response) {

                if (response.data.total == 0) {
                    news.notifications = null;
                } else {

                    news.notifications = response.data;
                }

                return news;
            });
        },

        initButtons: function () {

            $("#toggle-button").show();

            $("#main-menu-title").click(function () {

                $("#main-menu-icon").toggleClass("rotated");
                $(this).toggleClass("sidebar-subtitile-selected");

                if ($("#side-menu").hasClass("side-menu-collapsed")) {

                    $("#main-menu-container").toggle();
                    $("#second-menu-container").hide();

                } else {

                    $("#main-menu").slideToggle();
                }
            });

            $("#second-menu-title").click(function () {

                $("#second-menu-icon").toggleClass("rotated");
                $(this).toggleClass("sidebar-subtitile-selected");

                if ($("#side-menu").hasClass("side-menu-collapsed")) {

                    $("#second-menu-container").toggle();
                    $("#main-menu-container").hide();

                } else {

                    $("#second-menu").slideToggle();
                    $("#third-menu").slideToggle();
                    $("#fourth-menu").slideToggle();
                    $("#fifth-menu").slideToggle();
                    $("#sixth-menu").slideToggle();
                    $("#seventh-menu").slideToggle();
                }

            });

        }
    }
});

angular.module("sicc").directive("compile", function ($compile, $timeout) {
    return {

        link: function (scope, elem, attrs) {

            $timeout(function () {

                $compile(elem.contents())(scope);
            });
        }
    }
});

angular.module("sicc").controller("main-controller", function ($scope, $interval, principal, ngNotify) {

    ngNotify.addType("custom-not", "pop-notification");

    principal.checkSession().then(function (response) {

        activeUser = response.name;
        $scope.userImage = response.image;
        $scope.userName = response.name;
        $scope.userPosition = response.position;
        $scope.userEntity = response.entity;

    });

    principal.checkNews().then(function (response) {

        $scope.events = response.events;
        $scope.messagges = response.messagges;

        $scope.notifications = response.notifications;

        if (response.notifications != null) {

            $scope.numberNotifications = response.notifications.total;
        }

    });

    $interval(function () {

        principal.checkNews().then(function (response) {

            $scope.events = response.events;
            $scope.messagges = response.messagges;

            $scope.notifications = response.notifications;

            if (response.notifications != null) {

                if ($scope.numberNotifications == null) {
                    $scope.numberNotifications = 0;
                }

                if (response.notifications.total > $scope.numberNotifications) {

                    if ((response.notifications.total - $scope.numberNotifications) > 1) {

                        $("#noti-container").show();
                        ngNotify.set("Tiene nuevas notificaciones", {
                            position: "top",
                            duration: 3000,
                            type: "custom-not",
                            target: "#noti-container",
                            html: true
                        }, function () {
                            $("#noti-container").hide();

                        }, "alert");

                    } else {

                        $("#noti-container").show();
                        ngNotify.set(response.notifications.info[0].messagge, {
                            position: "top",
                            duration: 3000,
                            type: "custom-not",
                            target: "#noti-container",
                            html: true
                        }, function () {
                            $("#noti-container").hide();

                        }, response.notifications.info[0].img);
                    }
                }
                $scope.numberNotifications = response.notifications.total;
            } else {

                $scope.numberNotifications = null;
            }
        });

    }, 5000);


    // ---- JQUERY ---- //

    $("#page-loading").fadeOut("slow");
});

angular.module("sicc").controller("main-menu", function (principal) {

    principal.checkSession();

    $("#toggle-button").hide();
    $("#section-loading").fadeOut("slow");

});
