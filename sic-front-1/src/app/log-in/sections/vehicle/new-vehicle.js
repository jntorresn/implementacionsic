angular.module("sicc").controller("new-vehicle", function ($scope, $http, $location, principal) {

    principal.initButtons();

    $("#sub-button").popover({
        trigger: "manual"
    });

    $("input[name='creationDate']").val(moment().format("YYYY-MM-DD"));

    var datePickerConfig = {
        singleDatePicker: true,
        timePicker: false,
        opens: "center",
        format: "DD/MM/YYYY",
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10),
        locale: {
            format: "DD/MM/YYYY",
            applyLabel: "Seleccionar",
            cancelLabel: "Cancelar",
            fromLabel: "De",
            toLabel: "a",
            daysOfWeek: [
                "Dom",
                "Lun",
                "Mar",
                "Mie",
                "Jue",
                "Vie",
                "Sab"
            ],
            monthNames: [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            firstDay: 1
        }
    };

    if ($location.search().id != null) {

        getInformation();
        $scope.formAction = "Modficar Vehículo";
        $scope.imgAction = "modify";
        $scope.identifier = $location.search().id;

    } else {

        $scope.formAction = "Guardar Vehículo";
        $scope.imgAction = "next";
    }

    $("input[name='soat']").daterangepicker(datePickerConfig, function (start, end, label) {

        $scope.soat = start.format("DD/MM/YYYY");
        $("input[name='initialSoat']").val(start.format("YYYY-MM-DD"));

    });

    $("input[name='tm']").daterangepicker(datePickerConfig, function (start, end, label) {

        $scope.TM = start.format("DD/MM/YYYY");
        $("input[name='initialTm']").val(start.format("YYYY-MM-DD"));
    });

    $("input[name='photo']").change(function () {

        $("#nameFile").text($("input[name='photo']")[0].files[0].name);
        $("#sizeFile").text((($("input[name='photo']")[0].files[0].size) / 1000) + " KB");
    });

    $("a[title='Borrar Archivo']").click(function () {

        $("input[name='photo']").val(null);
        $("#nameFile").text(null);
        $("#sizeFile").text(null);
    });

    $("#sub-button").click(function () {

        var soatDate = new Date($("input[name='initialSoat']").val().replace("-", "/").replace("-", "/"));
        $("input[name='finalSoat']").val((soatDate.getFullYear() + 1) + "-" + (soatDate.getMonth() + 1) + "-" + soatDate.getDate());

        var tmDate = new Date($("input[name='initialTm']").val().replace("-", "/").replace("-", "/"));
        $("input[name='finalTm']").val((tmDate.getFullYear() + 1) + "-" + (tmDate.getMonth() + 1) + "-" + tmDate.getDate());

        if (validatedForm()) {

            if ($scope.formAction == "Guardar Vehículo") {

                $("#confirm-modal").addClass("show");
                $("#confirm-modal").show();

            } else {
                $("#password-modal").addClass("show");
                $("#password-modal").show();
            }

        } else {

            $("#sub-button").popover("show");
            setTimeout(function () {
                $("#sub-button").popover("hide");
            }, 4000);

        }

    });

    $("#close-modal").click(function () {

        $("#confirm-modal").removeClass("show");
        $("#confirm-modal").hide();
    });

    $("#confirm-btn").click(function () {

        $("#vehicle-form").submit();
    });

    $("#close-modify-modal").click(function () {

        $("#password-modal").removeClass("show");
        $("#password-modal").hide();
    });

    var tries = 0;

    $("#continue-button").click(function () {

        validateModal();
    });

    $("input[name='password']").keyup(function (e) {

        if (e.keyCode == 13) {
            validateModal();
        }
    });

    /// METHODS

    function validatedForm() {

        var validated = true;

        if (($scope.name == null) || ($scope.name == "")) {

            $("input[name='name']").addClass("input-missing");
            validated = false;
        } else {
            $("input[name='name']").removeClass("input-missing");
        }

        if ($location.search().id == null) {

            if ($("input[name='photo']").val() == "") {

                $("input[name='photo']").addClass("input-missing");
                validated = false;
            } else {
                $("input[name='photo']").removeClass("input-missing");
            }
        }

        if (($scope.vehicleType == null) || ($scope.vehicleType == "")) {

            $("select[name='vehicleType']").addClass("input-missing");
            validated = false;
        } else {
            $("select[name='vehicleType']").removeClass("input-missing");
        }

        if (($scope.brand == null) || ($scope.brand == "")) {

            $("input[name='brand']").addClass("input-missing");
            validated = false;
        } else {
            $("input[name='brand']").removeClass("input-missing");
        }

        if (($scope.model == null) || ($scope.model == "")) {

            $("input[name='model']").addClass("input-missing");
            validated = false;
        } else {
            $("input[name='model']").removeClass("input-missing");
        }

        if (($scope.plate == null) || ($scope.plate == "")) {

            $("input[name='plate']").addClass("input-missing");
            validated = false;
        } else {
            $("input[name='plate']").removeClass("input-missing");
        }

        if (($scope.soat == null) || ($scope.soat == "")) {

            $("input[name='soat']").addClass("input-missing");
            validated = false;
        } else {
            $("input[name='soat']").removeClass("input-missing");
        }

        if (($scope.tm == null) || ($scope.tm == "")) {

            $("input[name='tm']").addClass("input-missing");
            validated = false;
        } else {
            $("input[name='tm']").removeClass("input-missing");
        }

        return validated;
    }


    function getInformation() {

        $http({
            url: "inventory/vehicle/vehicle-controller.php",
            method: "GET",
            params: { id: $location.search().id }
        }).then(function (response) {

            $("#nameFile").text(response.data.imageName);

            $scope.name = response.data.name;
            $scope.vehicleType = response.data.vehicle_type;
            $scope.brand = response.data.brand;
            $scope.model = response.data.model;
            $scope.plate = response.data.plates;
            $scope.soat = response.data.SOAT_date;
            $scope.tm = response.data.TM_date;
            $("input[name='initialSoat']").val(response.data.SOAT_date);
            $("input[name='initialTm']").val(response.data.TM_date);
            $scope.photoID = response.data.imageID;
        });
    }

    function validateModal() {

        if (($scope.password == null) || ($scope.password == "")) {

            $("input[name='password']").addClass("input-missing");
        } else {

            $http({
                url: "../initial/user-controller.php",
                method: "get",
                params: {
                    password: $scope.password,
                    modify: true
                }
            }).then(function (response) {

                if (response.data == "enter") {
                    $("input[name='actionType']").val("modify");
                    $("#vehicle-form").submit();
                } else {
                    tries = tries + 1;
                    $scope.password = "";

                    if (tries == 3) {
                        alert("Su identidad no pudo ser confirmada.");
                        $("#logout-session").submit();
                    } else {
                        alert("Contraseña Incorrecta");
                    }
                }
            });
        }
    }

    $("#section-loading").fadeOut("slow");
});