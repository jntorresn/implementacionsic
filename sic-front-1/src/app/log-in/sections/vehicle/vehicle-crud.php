<?php

require_once "../../../conection.php";

Class VehicleCrud extends Conection {

    private $query;
    private $rows = array();

    public function get_vehicleTable() {

        $SQLString = "SELECT vehicle.id, inventory.name, vehicle.vehicle_type, vehicle.brand, vehicle.model, vehicle.plates, vehicle.SOAT_expiration, vehicle.TM_expiration
                        FROM inventory
                        INNER JOIN vehicle ON inventory.id = vehicle.inventory_id";
        $this->rows = $this->get_query($SQLString);
        return $this->rows;
    }

    public function createInventory($user, $creationDate, $inventoryType, $name) {
        $SQL_String = "INSERT INTO inventory(user, creation_date, inventory_type, name)
                        VALUES ('$user', '$creationDate', '$inventoryType', '$name')";
        $this->set_query($SQL_String);
    }

    public function getInventoryId($creationDate, $user) {
        $SQL_String = "SELECT inventory.id
                        FROM inventory
                        WHERE ((creation_date = '$creationDate') AND (user = '$user'))
                        ORDER BY inventory.id DESC";
        $this->rows = $this->get_query($SQL_String);
        return $this->rows;
    }

    public function getVehicleId($id_inventory) {
        $SQL_String = "SELECT vehicle.id 
                        FROM vehicle 
                        WHERE vehicle.inventory_id = $id_inventory";
        $this->rows = $this->get_query($SQL_String);
        return $this->rows;
    }

    public function createVehicle($photo, $inventoryId, $vehicleType, $brand, $model, $plates, $SOATDate, $TMDate, $SOATExpiration, $TMExpiration) {

        $SQL_String = "INSERT INTO vehicle(photo, inventory_id, vehicle_type, brand, model, plates, SOAT_date, TM_date, SOAT_expiration, TM_expiration)
                        VALUES ($photo, $inventoryId, '$vehicleType', '$brand', '$model', '$plates', '$SOATDate', '$TMDate', '$SOATExpiration', '$TMExpiration')";
        $this->set_query($SQL_String);
    }

    public function get_infoId($id) {

        $SQL_String = "SELECT image.id AS imageID, image.name AS imageName, image.file_path, inventory.id AS inventoryID, inventory.name, vehicle.vehicle_type, vehicle.brand, vehicle.model, vehicle.plates, vehicle.SOAT_expiration, vehicle.TM_expiration, vehicle.SOAT_date, vehicle.TM_date
                        FROM image
                        INNER JOIN vehicle ON image.id = vehicle.photo
                        INNER JOIN inventory ON vehicle.inventory_id = inventory.id
                        WHERE vehicle.id = $id";
        $this->rows = $this->get_query($SQL_String);
        return $this->rows;
    }

    public function update_vehicle($photo, $id, $type, $brand, $model, $plates, $SOATDate, $TMDate, $SOATExpiration, $TMExpiration) {

        $SQL_String = "UPDATE vehicle SET
                        photo = $photo,
                        vehicle_type = '$type',
                        brand = '$brand',
                        model = '$model',
                        plates = '$plates',
                        SOAT_date = '$SOATDate',
                        TM_date = '$TMDate',
                        SOAT_expiration = '$SOATExpiration',
                        TM_expiration = '$TMExpiration'
                        WHERE id = $id";
        echo ($SQL_String);
        $this->set_query($SQL_String);
    }

    public function checkInventoryId($id) {

        $SQL_String = "SELECT inventory.id
                        FROM inventory
                        INNER JOIN vehicle ON inventory.id = vehicle.inventory_id
                        WHERE vehicle.id = $id";
        $this->rows = $this->get_query($SQL_String);
        return $this->rows;
    }

    public function update_inventory($user, $creationDate, $inventoryType, $name, $id) {

        $SQL_String = "UPDATE inventory SET
                            user = '$user',
                            creation_date = '$creationDate',
                            inventory_type = '$inventoryType',
                            name = '$name'
                         WHERE id = $id";
        $this->set_query($SQL_String);
    }

    public function eliminate_vehicle($id_inventory) {
        $SQL_String = "DELETE FROM vehicle
                        WHERE inventory_id = $id_inventory";
        $this->set_query($SQL_String);

    }

    public function eliminate_photo($id_photo) {
        $SQL_String = "DELETE FROM image
                        WHERE id=$id_photo";
        $this->set_query($SQL_String);
    }

    public function eliminate_maintenance($id_inventory) {
        $SQL_String = "DELETE FROM inventory_maintenace
                        WHERE element_id=$id_inventory";
        $this->set_query($SQL_String);
    }

    public function eliminate_notifications($id_inventory) {
        $SQL_String = "DELETE FROM noti_inventory
                        WHERE inventory_id=$id_inventory";
        $this->set_query($SQL_String);
    }

    public function eliminate_inventory($id_inventory) {
        $SQL_String = "DELETE FROM inventory
                        WHERE id=$id_inventory";
        $this->set_query($SQL_String);
    }
}

?>