angular.module("sicc").controller("vehicle-info", function ($scope, $http, $location, principal) {

    principal.checkSession();
    principal.initButtons();

    $scope.ID = $location.search().id;

    $http({
        url: "inventory/vehicle/vehicle-controller.php",
        method: "GET",
        params: { id: $location.search().id }
    }).then(function (response) {

        $scope.imagePath = response.data.file_path;
        $scope.name = response.data.name;
        $scope.vehicleType = response.data.vehicle_type;
        $scope.brand = response.data.brand;
        $scope.model = response.data.model;
        $scope.plate = response.data.plates;
        $scope.soatExpiration = response.data.SOAT_expiration;
        $scope.tmExpiration = response.data.TM_expiration;

        $scope.imageID = response.data.imageID;
        $scope.inventoryID = response.data.inventoryID;
    });


    $("#eliminate-button").click(function () {

        $("#password-modal").addClass("show");
        $("#password-modal").show();
    });

    $("#close-modify-modal").click(function () {

        $("#password-modal").removeClass("show");
        $("#password-modal").hide();
    });

    var tries = 0;

    $("#continue-button").click(function () {

        validateModal();
    });

    $("input[name='password']").keyup(function (e) {

        if (e.keyCode == 13) {
            validateModal();
        }
    });

    function validateModal() {

        if (($scope.password == null) || ($scope.password == "")) {

            $("input[name='password']").addClass("input-missing");
        } else {

            $http({
                url: "../initial/user-controller.php",
                method: "get",
                params: {
                    password: $scope.password,
                    modify: true
                }
            }).then(function (response) {

                if (response.data == "enter") {
                    $("#eliminate-form").submit();
                } else {
                    tries = tries + 1;
                    $scope.password = "";

                    if (tries == 3) {
                        alert("Su identidad no pudo ser confirmada.");
                        $("#logout-session").submit();
                    } else {
                        alert("Contraseña Incorrecta");
                    }
                }
            });
        }
    }

    $("#section-loading").fadeOut("slow");
});