angular.module("sicc").controller("vehicles", function ($scope, $http, principal, DTOptionsBuilder, DTColumnBuilder) {

    principal.checkSession();
    principal.initButtons();

    var language = {
        "sEmptyTable": "No hay datos disponibles en la tabla.",
        "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros.",
        "sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
        "sInfoFiltered": "(filtrado de _MAX_ total registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ",",
        "sLengthMenu": "Mostrando _MENU_ registros",
        "sLoadingRecords": "Cargando...",
        "sProcessing": "Procesando...",
        "sSearch": "Buscar:",
        "sZeroRecords": "No se encontraron registros que coincidan.",
        "oPaginate": {
            "sFirst": "Primera",
            "sLast": "Última",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": activar para ordenar la columna de forma ascendente",
            "sSortDescending": ": activar para ordentar la columna de forma descendente"
        }
    };


    $http.get("inventory/vehicle/vehicle-controller.php").then(function (response) {

        $scope.rows = response.data;


        $scope.dtOptions = DTOptionsBuilder.fromSource()
            .withLanguage(language)
            .withColumnFilter({
                aoColumns: [{
                    type: 'number',
                    sWidth: "20px"
                }, {
                    type: 'text'
                }, {
                    type: 'text'
                }, {
                    type: 'text'
                }, {
                    type: 'text'
                }, {
                    type: 'text'
                }, {
                    type: 'text'
                }, {
                    type: 'text'
                }]
            });
    });

    $("#section-loading").fadeOut("slow");
});