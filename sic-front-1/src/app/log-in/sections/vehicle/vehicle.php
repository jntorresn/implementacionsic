<?php

require_once "vehicle-crud.php";
require_once "../inventory.php";

class Vehicle extends Inventory {

    private $photo;
    private $type;
    private $brand;
    private $model;
    private $plates;
    private $SOATDate;
    private $TMDate;
    private $SOATExpiration;
    private $TMExpiration;

    public function getPhoto() {
        return $this->photo;
    }
    public function setPhoto($photo) {
        $this->photo = $photo;
    }
    public function getType() {
        return $this->type;
    }
    public function setType($type) {
        $this->type = $type;
    }
    public function getBrand() {
        return $this->brand;
    }
    public function setBrand($brand) {
        $this->brand = $brand;
    }
    public function getModel() {
        return $this->model;
    }
    public function setModel($model) {
        $this->model = $model;
    }
    public function getPlates() {
        return $this->plates;
    }
    public function setPlates($plates) {
        $this->plates = $plates;
    }
    public function getSOATDate() {
        return $this->SOATDate;
    }
    public function setSOATDate($SOATDate) {
        $this->SOATDate = $SOATDate;
    }
    public function getTMDate() {
        return $this->TMDate;
    }
    public function setTMDate($TMDate) {
        $this->TMDate = $TMDate;
    }
    public function getSOATExpiration() {
        return $this->SOATExpiration;
    }
    public function setSOATExpiration($SOATExpiration) {
        $this->SOATExpiration = $SOATExpiration;
    }
    public function getTMExpiration() {
        return $this->$TMExpiration;
    }
    public function setTMExpiration($TMExpiration) {
        $this->TMExpiration = $TMExpiration;
    }

    public function getVehicleTable() {

        $crud = new VehicleCrud();

        return json_encode($crud->get_VehicleTable());
    }

    public function saveVehicle() {

        $crud = new VehicleCrud();
        $rows = array();

        $crud->createInventory($this->user, $this->creationDate, $this->inventoryType, $this->name);
        $rows = $crud->getInventoryId($this->creationDate, $this->user);
        $this->id = $rows[0]["id"];
        $crud->createVehicle($this->photo, $rows[0]["id"], $this->type, $this->brand, $this->model, $this->plates, $this->SOATDate, $this->TMDate, $this->SOATExpiration, $this->TMExpiration);

        $rows = $crud->getVehicleId($this->id);
        unset($crud);

        return $rows[0]["id"];

    }

    public function getInfoId($id) {

        $crud = new VehicleCrud();

        $rows = array();

        $rows = $crud->get_infoId($id);

        return json_encode($rows[0]);
        unset($crud);
    }

    public function updateVehicle($id) {

        $crud = new VehicleCrud();
        $rows = array();

        $crud->update_vehicle($this->photo, $id, $this->type, $this->brand, $this->model, $this->plates, $this->SOATDate, $this->TMDate, $this->SOATExpiration, $this->TMExpiration);
        $rows = $crud->checkInventoryId($id);
        $this->id = $rows[0]["id"];
        $crud->update_inventory($this->user, $this->creationDate, $this->inventoryType, $this->name, $this->id);
    }

    public function eliminateElement($id_inventory, $id_photo) {

        $crud = new VehicleCrud();

        $crud->eliminate_vehicle($id_inventory);
        $crud->eliminate_photo($id_photo);
        $crud->eliminate_maintenance($id_inventory);
        $crud->eliminate_notifications($id_inventory);
        $crud->eliminate_inventory($id_inventory);

        unset($crud);

    }
}

?>