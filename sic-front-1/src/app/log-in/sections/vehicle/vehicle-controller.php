<?php

require_once "vehicle.php";
require_once "../../images/images.php";
require_once "../../../initial/user/user.php";
require_once "../../notification/notification-model.php";
require_once "../../notification/notification-user-model.php";

$vehicle = new Vehicle();

if (isset($_POST["actionType"])) {

    switch ($_POST["actionType"]) {
    case "creation":

        if ($_FILES["photo"]["tmp_name"] != null) {

            $image = new Image();

            $image->setName("Foto-" . $_POST["name"]);
            $image->setPath("dist\/img\/inventory\/vehicles/" . $image->getName() . ".jpg");
            $image->setUse("Vehículo de Inventario");

            $image->saveImage();

            $_FILES["photo"]["name"] = "Foto-" . $_POST["name"] . ".jpg";

            $file = $_FILES["photo"]["tmp_name"];
            $path = "../../../dist/img/inventory/vehicles/" . $_FILES["photo"]["name"];

            if (!file_exists("../../../dist/img/inventory")) {
                mkdir("../../../dist/img/inventory", 0777, true);
            }
            if (!file_exists("../../../dist/img/inventory/vehicles")) {
                mkdir("../../../dist/img/inventory/vehicles", 0777, true);
            }

            if (file_exists($path)) {

                unlink($path);
            }

            move_uploaded_file($file, $path);

            $vehicle->setPhoto($image->getId());

            unset($image);
        }

        session_start();

        $vehicle->setUser($_SESSION["user"]);
        $vehicle->setCreationDate($_POST["creationDate"]);
        $vehicle->setInventoryType($_POST["inventoryType"]);
        $vehicle->setName($_POST["name"]);

        $vehicle->setType($_POST["vehicleType"]);
        $vehicle->setBrand($_POST["brand"]);
        $vehicle->setModel($_POST["model"]);
        $vehicle->setPlates($_POST["plate"]);
        $vehicle->setSOATDate($_POST["initialSoat"]);
        $vehicle->setSOATExpiration($_POST["finalSoat"]);
        $vehicle->setTMDate($_POST["initialTm"]);
        $vehicle->setTMExpiration($_POST["finalTm"]);

        $vehicleID = $vehicle->saveVehicle();

        ///////// NOTIFICATIONS

        //--- Notification ---//

        $new_notification = new Notification();

        $new_notification->setPriority("Baja");

        $new_notification->setMessagge("Nuevo vehículo agregado");

        $new_notification->setDistribution("Supervisor Interventoría");
        $new_notification->setCreationDate($_POST["creationDate"]);
        $new_notification->setSection("Inventario");
        $new_notification->setType("user");

        $new_notification->saveNotificationInventory($vehicle->getId(), $_SESSION["user"]);

        $users = array();

        $users_target = new User();
        $users = $users_target->getNumberUsers("Supervisor Interventoría");

        for ($i = 0; $i < count($users); $i++) {

            $noti_user = new NotificationUser();

            $noti_user->setUser($users[$i]["name"]);
            $noti_user->setId($new_notification->getId());
            $noti_user->setSeen(0);

            $noti_user->saveRecord();
        }

        unset($new_notification);

        header("Location: ../../principal.html#!/vehicle-info?id=" . $vehicleID);

        break;

    case "modify":

        if ($_FILES["photo"]["tmp_name"] != null) {

            $image = new Image();

            $image->setName("Foto-" . $_POST["name"]);
            $image->setPath("dist\/img\/inventory\/vehicles/" . $image->getName() . ".jpg");
            $image->setUse("Vehículo de Inventario");

            $image->updatePhoto($_POST["identifier"], "vehicle");

            $_FILES["photo"]["name"] = "Foto-" . $_POST["name"] . ".jpg";

            $file = $_FILES["photo"]["tmp_name"];
            $path = "../../../dist/img/inventory/vehicles/" . $_FILES["photo"]["name"];

            if (!file_exists("../../../dist/img/inventory")) {
                mkdir("../../../dist/img/inventory", 0777, true);
            }
            if (!file_exists("../../../dist/img/inventory/vehicles")) {
                mkdir("../../../dist/img/inventory/vehicles", 0777, true);
            }

            if (file_exists($path)) {

                unlink($path);
            }

            move_uploaded_file($file, $path);

            $vehicle->setPhoto($image->getId());

            unset($image);
        } else {

            $vehicle->setPhoto($_POST["photoID"]);
        }

        session_start();

        $vehicle->setUser($_SESSION["user"]);
        $vehicle->setCreationDate($_POST["creationDate"]);
        $vehicle->setInventoryType($_POST["inventoryType"]);
        $vehicle->setName($_POST["name"]);

        $vehicle->setType($_POST["vehicleType"]);
        $vehicle->setBrand($_POST["brand"]);
        $vehicle->setModel($_POST["model"]);
        $vehicle->setPlates($_POST["plate"]);
        $vehicle->setSOATDate($_POST["initialSoat"]);
        $vehicle->setSOATExpiration($_POST["finalSoat"]);
        $vehicle->setTMDate($_POST["initialTm"]);
        $vehicle->setTMExpiration($_POST["finalTm"]);

        $vehicle->updateVehicle($_POST["identifier"]);

        ///////// NOTIFICATIONS

        //--- Notification ---//

        $new_notification = new Notification();

        $new_notification->setPriority("Baja");

        $new_notification->setMessagge("Vehículo Modificado");

        $new_notification->setDistribution("Supervisor Interventoría");
        $new_notification->setCreationDate($_POST["creationDate"]);
        $new_notification->setSection("Inventario");
        $new_notification->setType("user");

        $new_notification->saveNotificationInventory($vehicle->getId(), $_SESSION["user"]);

        $users = array();

        $users_target = new User();
        $users = $users_target->getNumberUsers("Supervisor Interventoría");

        for ($i = 0; $i < count($users); $i++) {

            $noti_user = new NotificationUser();

            $noti_user->setUser($users[$i]["name"]);
            $noti_user->setId($new_notification->getId());
            $noti_user->setSeen(0);

            $noti_user->saveRecord();
        }

        unset($new_notification);

        header("Location: ../../principal.html#!/vehicle-info?id=" . $_POST["identifier"]);

        break;

        break;
    default:
        echo ("Enter Default");
        break;
    }
} elseif (isset($_GET["id"])) {

    echo ($vehicle->getInfoId($_GET["id"]));

} elseif (isset($_POST["eliminate"])) {

    $vehicle->eliminateElement($_POST["eliminate"], $_POST["imageID"]);
    header("Location: ../../principal.html#!/vehicles");

} else {
    echo ($vehicle->getVehicleTable());
}

unset($vehicle);

?>