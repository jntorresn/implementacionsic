<?php

class Inventory {

    protected $id;
    protected $user;
    protected $creationDate;
    protected $inventoryType;
    protected $name;

    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }
    public function getUser() {
        return $this->user;
    }
    public function setUser($user) {
        $this->user = $user;
    }
    public function getCreationDate() {
        return $this->creationDate;
    }
    public function setCreationDate($creationDate) {
        $this->creationDate = $creationDate;
    }
    public function getInventoryType() {
        return $this->inventoryType;
    }
    public function setInventoryType($inventoryType) {
        $this->inventoryType = $inventoryType;
    }
    public function getName() {
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }

}